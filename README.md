# Internet Sentiment Analysis

Lexicon-based multilingual sentiment analysis for internet conversations.

Part of the module that deals with Czech and CzechWithoutDiacritics sentiment analysis is based on the following master theses: https://is.muni.cz/auth/publication/1602459/cs

The remainder of the module relies on the polyglot module: https://polyglot.readthedocs.io/en/latest/

## Dependencies
 - Python >= 3.6
 - polyglot >= 16.7.4
 - iso639 >= 0.1.4
 - unidecode

## Setup
The following set of commands clones the repository and installs the required dependencies
```sh
$ git clone https://gitlab.fi.muni.cz/nlp/internet-sentiment-analysis.git
$ cd internet-sentiment-analysis
$ pip install -r requirements
```

## Usage

### Czech sentiment analysis module
 - separate module located in `emotions.py` 

Example Usage:
```py
from emotions import Text

t = Text("to je moc pěkný")
t.printEmotions()
```

Output:
```py
POS 100.0
NEG 0.0
NEU 0.0
```

### Czech sentiment analysis module
 - a wrapper around `polyglot` and aforementioned module

Example Usage:
```py
from internet_sentiment_analysis import SentimentAnalyzer

analyzer = SentimentAnalyzer()
print(analyzer.detect_from_message("to je moc pekny", language="csd")[0])
```

Output:
```py
1.0
```
- if language isn't specified, `polyglot` will attempt to identify it using its internal module (not recommended)


### Evaluation
 - dataset of 365 messages with annotated sentiment can be found in `eval/sentiment-test-set.txt`
 - Makefile contains 2 targets starting with `evaluate_` that evaluate either polyglot or this module against this dataset
