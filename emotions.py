#! /usr/bin/env python3
import sys
from subprocess import Popen, PIPE, STDOUT
import re
TAG_REG = re.compile("<.*>") 
DIR = "/nlp/projekty/officebot/trac-git/officebot_dev/sentiment/"
FILES_FN = ["affin_cz_smooth.csv", "eval_words_translated.csv", "new-negative.csv", "new-positive.csv", "politics.csv", "changing.csv"]
FILES = []
for fn in FILES_FN:
    if sys.version_info[0] < 3:
        flines = [x.decode('utf-8') for x in open(DIR + fn).readlines()]
    else:
        flines = open(DIR + fn, 'r', encoding='utf-8').readlines()
    FILES.append(flines)

WORDS = {}

for emotion_file in FILES:
    for line in emotion_file:
        word, polarity, intensity = line.split(",")
        WORDS[word] = (polarity, float(intensity))


del FILES


NEG_EMOJI_SCORE=2
NEG_EMO_COEFF=1.5
POS_RAW_COEFF=1
ENOUGH_INFO=5


SEPARATOR = ','
NEGATIVESMILES = {":-(", ":(", ">:(", ">:-(", ":,-(", ":,(", ":'-(", ":'(", ":-/", ":/", ":-(*)", ":(*)", "(_!_)", "(.)(.)", "( . Y . )", "(N)", "(n)",
                ":worried:",
                ":anguished:",
                ":confused:",
                ":unamused:",
                ":disappointed_relieved:",
                ":weary:",
                ":disappointed:",
                ":confounded:",
                ":fearful:",
                ":cold_sweat:",
                ":cry:",
                ":sob:",
                ":scream:",
                ":tired_face:",
                ":angry:",
                ":rage:",
                ":dizzy_face:",
                ":broken_heart:",
                ":dizzy:",
                ":anger:",
                ":shit:",
                ":-1:",
                ":thumbsdown:",
                ":punch:",
                ":fist:",
                ":scream_cat:",
                ":see_no_evil:",
                ":hear_no_evil:",
                ":speak_no_evil:",
                ":bust_in_silhouette:",
                ":busts_in_silhouette:",
                ":rage1:",
                ":rage2:",
                ":rage3:",
                ":rage4:",
                ":suspect:",
                ":cloud:",
                ":pig:",
                ":pig_nose:",
                ":bug:",
                ":pig2:",
                ":fallen_leaf:",
                ":waning_gibbous_moon:",
                ":waning_crescent_moon:",
                ":alarm_clock:",
                ":bomb:",
                ":space_invader:",
                ":black_joker:",
                ":game_die:",
                ":fork_and_knife:",
                ":fried_shrimp:",
                ":stew:",
                ":lemon:",
                ":light_rail:",
                ":warning:",
                ":checkered_flag:",
                ":arrow_backward:",
                ":twisted_rightwards_arrows:",
                ":vibration_mode:",
                ":negative_squared_cross_mark:"}
POSITIVESMILES = {":-)", ":)", ":-p", ":p", ":-P", ":P", ":-d", ":-D", ":d", ":D", "x-D", "xD", "XD", "X-D", ";-)", ";)", ";-D", ";D", "(Y)", "(y)",
                ":smile:",
                ":slightly_smiling_face:",
                ":simple_smile:",
                ":laughing:",
                ":smiley:",
                ":relaxed:",
                ":heart_eyes:",
                ":kissing_heart:",
                ":kissing_closed_eyes:",
                ":satisfied:",
                ":grin:",
                ":stuck_out_tongue_winking_eye:",
                ":stuck_out_tongue_closed_eyes:",
                ":kissing_smiling_eyes:",
                ":stuck_out_tongue:",
                ":grimacing:",
                ":sweat_smile:",
                ":persevere:",
                ":joy:",
                ":astonished:",
                ":triumph:",
                ":smiling_imp:",
                ":yellow_heart:",
                ":blue_heart:",
                ":purple_heart:",
                ":heart:",
                ":green_heart:",
                ":sparkling_heart:",
                ":boom:",
                ":+1:",
                ":thumbsup:",
                ":point_right:",
                ":no_good:",
                ":angel:",
                ":smile_cat:",
                ":joy_cat:",
                ":love_letter:",
                ":cherry_blossom:",
                ":blossom:",
                ":mag_right:",
                ":gem:",
                ":trophy:",
                ":trumpet:",
                ":peach:",
                ":sweet_potato:",
                ":convenience_store:",
                ":love_hotel:",
                ":statue_of_liberty:",
                ":arrow_lower_right:",
                ":arrow_right:",
                ":arrow_upper_right:",
                ":arrow_right_hook:",
                ":left_right_arrow:",
                ":fast_forward:",
                ":top:",
                ":cool:",
                ":free:",
                ":congratulations:",
                ":ideograph_advantage:",
                ":sparkle:"}
if sys.version_info[0] < 3:
    SHORT = [x.decode('utf-8') for x in open(DIR + "shorts.csv", 'r').readlines()]
    MONTHS = [x.decode('utf-8') for x in open(DIR + "months.csv", 'r').readlines()]
else:
    SHORT = open(DIR + "shorts.csv", 'r', encoding='utf-8').readlines()
    MONTHS = open(DIR + "months.csv", 'r', encoding='utf-8').readlines()
POLITICS = {"ODS", "SPOZ", "ANO"}


#preprocess smiles
def preprocess_smiles(smiles):
    smiles = list(smiles)
    for i in range(len(smiles)):
        if smiles[i][0] == ":" and smiles[i][-1] == ":":
            smiles[i] = smiles[i][1:-1]
    print(smiles)
    return set(smiles)

#POSITIVESMILES = preprocess_smiles(POSITIVESMILES)
#NEGATIVESMILES = preprocess_smiles(NEGATIVESMILES)

class Text:
    def __init__(self, input):
        self.sentences = []
        self.emotions = dict(p=0,n=0)
        self.rawEmotions = dict(p=0,n=0)
        self.possibleEmotions = dict(p=0,n=0,z=0)
        self.processedInput = input
        self.isSpaced = False
        self.numOfTotalWords = 0
        self.numOfAffectedWords = 0
        self.numOfCapsLocks = 0
        self.numOfPositiveSmiles = 0
        self.numOfNegativeSmiles = 0
        self.numOfQuestionMarks = 0
        self.numOfPowerMarks = 0
        self.numOfSentences = 0
        self.recognizeSentences()
        self.computeEmotions()
        #self.printEmotions()

    def recognizeSentences(self):
        self.processedInput += " "
        self.preProcessRawInput()
        sntc = ""
        wordCntr = 0
        i = 0
        for c in self.processedInput:
            if (c == '.' or c == '?' or c == '!'):
                if (self.processedInput[i+1] == c):
                    sntc += c
                    continue
                if (self.processedInput[i-1].isdigit()):
                    sntc += c
                    continue
                if (wordCntr > 5):
                    if ((self.isSpaced == True and self.processedInput[i+1].isspace()) or self.isSpaced == False):
                        if (self.isSpaced):
                            sntc = sntc.replace('.','TTTTT')
                            sntc = sntc.replace('*','HHHHH')
                        sntc += c
                        if(not sntc.isspace()):
                            self.sentences.append(Sentence(sntc.strip()))
                            self.numOfSentences += 1
                            sntc = ""
                            wordCntr = 0
                else:
                    sntc += c
            elif (c.isspace()):
                if (wordCntr > 35): #prumerny pocet slov na vetu * 2
                    if(not sntc.isspace()):
                        self.sentences.append(Sentence(sntc))
                        self.numOfSentences += 1
                        sntc = ""
                        wordCntr = 0
                else:
                    sntc += ' '
                    wordCntr += 1
            else:
                sntc += c
            i += 1
        if(sntc != ""):
            if(not sntc.isspace()):
                self.sentences.append(Sentence(sntc))
                self.numOfSentences += 1

    def preProcessRawInput(self):
        self.processedInput = self.processedInput.replace('\t',' ')
        self.processedInput = self.processedInput.replace('\n',' ')
        self.processedInput = self.processedInput.replace('\r\n',' ')
        self.processedInput = self.processedInput.strip()
        self.processedInput += " "

        for pSmile in POSITIVESMILES:
            while(pSmile in self.processedInput):
                self.processedInput = self.processedInput.replace(pSmile, "POSITIVESMILE", 1)
        for nSmile in NEGATIVESMILES:
            while(nSmile in self.processedInput):
                self.processedInput = self.processedInput.replace(nSmile, "NEGATIVESMILE", 1)
        self.processedInput = self.processedInput.replace("\"", " USIGN ")
        self.processedInput = self.processedInput.replace("(", " OBORDER ")
        self.processedInput = self.processedInput.replace(")", " CBORDER ")
        self.processedInput = self.processedInput.replace("  ", " ")
        for shrt in SHORT:
            replString = shrt + '.'
            self.processedInput = self.processedInput.replace(replString, shrt)
        for day in range(1,32):
            for month in range(1,13):
                self.processedInput = self.processedInput.replace((str(day) + "." + str(month) + "."), 'dateInfo')
                self.processedInput = self.processedInput.replace((str(day) + ". " + str(month) + "."), 'dateInfo')
                self.processedInput = self.processedInput.replace((str(day) + "." + str(month)), 'dateInfo')
                self.processedInput = self.processedInput.replace((str(day) + ". " + str(month)), 'dateInfo')
            for month in MONTHS:
                self.processedInput = self.processedInput.replace((str(day) + "." + month), 'dateInfo')
                self.processedInput = self.processedInput.replace((str(day) + ". " + month), 'dateInfo')
        if sys.version_info[0] < 3:
            sentences = unicode.count(self.processedInput,". ") + unicode.count(self.processedInput,"! ") + unicode.count(self.processedInput,"? ")
            shorts = unicode.count(self.processedInput,".") + unicode.count(self.processedInput,"!") + unicode.count(self.processedInput,"?")
        else:
            sentences = str.count(self.processedInput,". ") + str.count(self.processedInput,"! ") + str.count(self.processedInput,"? ")
            shorts = str.count(self.processedInput,".") + str.count(self.processedInput,"!") + str.count(self.processedInput,"?")
        if (sentences >= shorts*2):
            self.isSpaced = True

    def computeEmotions(self):
        for sentence in self.sentences:
            #sentence.printStatistics()
            if (sentence.getBestEmotion()[0] != 'Z'):
                self.emotions[sentence.getBestEmotion()[0]] += sentence.getBestEmotion()[1]
            self.rawEmotions['n'] += sentence.getAllEmotions()['n']
            self.rawEmotions['p'] += sentence.getAllEmotions()['p']
            self.numOfAffectedWords += sentence.getNumOfAffectedWords()
            self.numOfCapsLocks += sentence.getNumOfUpperWords()
            self.numOfPowerMarks += sentence.getNumOfPowerMarks()
            self.numOfQuestionMarks += sentence.getNumOfQuestionMarks()
            self.numOfPositiveSmiles += sentence.getNumOfPositiveSmiles()
            self.numOfNegativeSmiles += sentence.getNumOfNegativeSmiles()
            self.numOfTotalWords += sentence.getNumOfWords()
        if ((self.rawEmotions['n']+self.rawEmotions['p']) == 0 or self.numOfAffectedWords == 0):
            self.possibleEmotions['p'] = 0
            self.possibleEmotions['n'] = 0
            self.possibleEmotions['z'] = 100
            return
        self.emotions['n'] = self.emotions['n']/self.numOfSentences
        self.emotions['p'] = self.emotions['p']/self.numOfSentences
        self.rawEmotions['n'] = (100/float(self.numOfAffectedWords))*self.rawEmotions['n']
        self.rawEmotions['p'] = (100/float(self.numOfAffectedWords))*self.rawEmotions['p']
        self.emotions['n'] += 2*(100/(self.rawEmotions['n']+self.rawEmotions['p'])*self.rawEmotions['n'])
        self.emotions['n'] = self.emotions['n']/3
        self.emotions['p'] += 2*(100/(self.rawEmotions['n']+self.rawEmotions['p'])*self.rawEmotions['p'])
        self.emotions['p'] = self.emotions['p']/3
        negativeCoef = self.computeNegativeCoef()
        positiveCoef = self.computePositiveCoef()
        self.possibleEmotions['p'] = self.emotions['p']
        self.possibleEmotions['n'] = self.emotions['n']
        self.possibleEmotions['z'] = 100-self.emotions['n'] - self.emotions['p']

    def printEmotions(self):
        sortedEmotions = sorted(self.possibleEmotions.items(), key=lambda x: x[1],reverse=True)
        for emotion in sortedEmotions:
            print ({"n": "NEG", "p": "POS", "z": "NEU"}[emotion[0]], str(round(emotion[1],2)))
                
    def computeNegativeCoef(self):
        coef = self.rawEmotions['n']/(self.numOfTotalWords)
        #affected = self.numOfCapsLocks + self.rawEmotions['n'] + self.numOfPowerMarks + self.numOfQuestionMarks + self.numOfNegativeSmiles
        affected = self.rawEmotions['n'] + self.numOfPowerMarks + self.numOfQuestionMarks + self.numOfNegativeSmiles
        return coef*affected

    def computePositiveCoef(self):
        coef = self.rawEmotions['p']/(self.numOfTotalWords * POS_RAW_COEFF)
        affected = self.numOfPositiveSmiles + self.rawEmotions['p']
        return coef*affected


class Sentence:
    def __init__(self, input):
        self.words = []
        self.emotions = dict(p=0,n=0)
        self.numOfAffectedWords = 0
        self.numOfWords = 0
        self.numOfUpperWords = 0
        self.questionMarks = 0
        self.powerMarks = 0
        self.numOfPositiveSmiles = 0
        self.numOfNegativeSmiles = 0
        self.willNotAffect = True
        self.defaultSentence = ""
        self.getWords(input)
        self.computeEmotions()

    def getNumOfAffectedWords(self):
        return self.numOfAffectedWords

    def getNumOfWords(self):
        return self.numOfWords

    def getNumOfUpperWords(self):
        return self.numOfUpperWords

    def getNumOfQuestionMarks(self):
        return self.questionMarks

    def getNumOfPowerMarks(self):
        return self.powerMarks

    def getNumOfPositiveSmiles(self):
        return self.numOfPositiveSmiles

    def getNumOfNegativeSmiles(self):
        return self.numOfNegativeSmiles

    def getWords(self, rawSntc):
        self.defaultSentence = rawSntc
        self.preProcessSntc()
        if(self.defaultSentence.isspace()):
            return
        wordArray = self.getDefaultFormat(self.defaultSentence)
        i = 0
        for wrd in wordArray:
            self.preProcessWrd(wrd)
            self.words.append(Word(wrd))
            i += 1
        self.numOfWords = i
    
    def preProcessSntc(self):
        while("POSITIVESMILE" in self.defaultSentence):
            self.emotions['p'] += 1
            self.numOfPositiveSmiles += 1
            self.numOfAffectedWords += 1
            self.defaultSentence = self.defaultSentence.replace("POSITIVESMILE", '', 1)
            self.willNotAffect = False
        while("NEGATIVESMILE" in self.defaultSentence):
            self.emotions['n'] += NEG_EMOJI_SCORE
            self.numOfNegativeSmiles += 1
            self.numOfAffectedWords += 1
            self.defaultSentence = self.defaultSentence.replace("NEGATIVESMILE", '', 1)
            self.willNotAffect = False
        if(self.defaultSentence.isspace()):
            return
        question = "??"
        questionCntr = self.defaultSentence.count(question)
        for i in range(0,5):
            question += "?"
            newCnt = self.defaultSentence.count(question)
            if(newCnt < questionCntr and newCnt*2 >= questionCntr):
                questionCntr = newCnt
            else:
                self.questionMarks = questionCntr
                break
        power = "!!"
        powerCntr = self.defaultSentence.count(power)
        for i in range(0,5):
            power += "!"
            newCnt = self.defaultSentence.count(power)
            if(newCnt < powerCntr and newCnt*2 >= powerCntr):
                powerCntr = newCnt
            else:
                self.powerMarks = powerCntr
                break

    def printStatistics(self):
        print(self.defaultSentence)
        print("Number of words: " + str(self.numOfWords))
        print("Number of affective words: " + str(self.numOfAffectedWords))
        print("Number of CAPS LOCK's words: " + str(self.numOfUpperWords))
        print("Number of question mark strings: " + str(self.questionMarks))
        print("Number of power mark strings: " + str(self.powerMarks))
        print("Number of positive smiles: " + str(self.numOfPositiveSmiles))
        print("Number of negative smiles: " + str(self.numOfNegativeSmiles))
        print("Positive score: " + str(self.getAllEmotions()['p']))
        print("Negative score: " + str(self.getAllEmotions()['n']))
        print("Best emotion: " + str(self.getBestEmotion()[0]) + " with score: " + str(self.getBestEmotion()[1]))

    def preProcessWrd(self, wrd):
        if(self.defaultSentence.isspace()):
            return
        """
        if ("USIGN " + wrd[0] + " USIGN" in self.defaultSentence):
            self.emotions['n'] += 2
            self.numOfAffectedWords += 1
        if (wrd[0].isupper()):
            if (wrd[0] not in POLITICS):
                self.numOfUpperWords += 1
                self.emotions['n'] += 2
                self.numOfAffectedWords += 1
                self.willNotAffect = False
        """

    def getAllEmotions(self):
        return self.emotions
    
    def setEmotions(self, emo):
        self.emotions = emo

    def getBestEmotion(self): #TODO-vyladit koeficienty, prva podmienka rovnaka ale bez floatu, prostredna podmienka sa vynecha
        bestEmotion = ('Z', 100)
        if (self.hasEnoughInfo()):
            if (self.emotions['p'] > self.emotions['n']):
                perc = self.computePercentil(self.emotions['p'], self.emotions['n'], self.emotions['p'])
                bestEmotion = ('p', perc)
            #elif (self.emotions['p']*2 < self.emotions['n']):
            #    perc = self.computePercentil(self.emotions['p'], self.emotions['n'], self.emotions['n'])
            #    bestEmotion = ('n', perc)
            else:
                perc = self.computePercentil(self.emotions['p'], self.emotions['n']*NEG_EMO_COEFF, self.emotions['n']*NEG_EMO_COEFF)
                bestEmotion = ('n', perc)

        return bestEmotion

    def computePercentil(self, first, second, whatPerc):
        if (whatPerc == 0):
            return 0
        if ((first + second) == 0):
            return 0
        return (100/(first + second)*whatPerc)

    def computeEmotions(self):
        if(self.defaultSentence.isspace()):
            return
        numOfAffectedWords = 0
        index = 0
        doubleNext = False
        delDoubleNext = False
        reverse = False
        for wrd in self.words:
            if (wrd.getEmotion()[0] != 'Z'):
                self.numOfAffectedWords += 1
            if (doubleNext):
                delDoubleNext = True
            for i,c in enumerate(wrd.getEmotion()):
                if (c == 'p' or c == 'n'):
                    toAdd = float(wrd.getEmotionLevel()[i])
                    if (doubleNext):
                        toAdd += toAdd
                    self.emotions[c] += toAdd
                elif (c == '+'):
                    doubleNext = True
                elif (c == '!' and self.willNotAffect):
                    reverse = not reverse
                else:
                    continue
            if (delDoubleNext):
                doubleNext = False
                delFoubleNext = False
            index += 1
        if (reverse):
            helpInt = self.emotions['p']
            self.emotions['p'] = self.emotions['n']
            self.emotions['n'] = helpInt

    def getSavedWords(self):
        return self.words

    def hasEnoughInfo(self):
        if (self.numOfAffectedWords * 10 > self.numOfWords):
            return True
        if (self.emotions['p'] > ENOUGH_INFO or self.emotions['n'] > ENOUGH_INFO):
            return True
        return False

    @staticmethod
    def getDefaultFormat(text):
        p = Popen(["/usr/bin/env python2 /corpora/programy/unitok.py | /corpora/programy/desamb.utf8.sh -skipdis"], shell=True, stdin=PIPE, stdout=PIPE)
        out = p.communicate(input=text.encode('utf-8'))[0]
        #if sys.version_info[0] < 3:
        out = out.decode('utf-8')
        #print ("out='{}'".format(repr(out)))
        pairs = []
        for line in out.strip().split("\n"):
            if TAG_REG.match(line):
                continue
            spl = line.split("\t")
            if len(line) < 3:
                continue
            try:
                w, lemma, tag = spl[0], spl[1], spl[2]
            except Exception as e:
                print(str(e), line)
                import sys
                sys.exit(1)
            pairs.append((w, lemma, tag))
        return pairs
        
class Word:
    def __init__(self, input):
        self.rawWord = input[0].strip()
        self.word = input[1].strip()
        self.tags = input[2].strip()
        self.emotion = []
        self.emotionLevel = []
        self.findEmotion()

    def getEmotion(self):
        return self.emotion

    def getEmotionLevel(self):
        return self.emotionLevel


    def noSpaceEmotion(self, line, lineWrd):
        self.rawWord = self.rawWord.replace(' ', '')
        self.word = self.word.replace(' ','')
        if (self.rawWord == lineWrd) or (self.word == lineWrd): #pokud je slovo nalezeno na radku
            self.getAndSaveEmotion(line)
            return True
        for switchedWord in self.getSwitched(self.rawWord):
            if (switchedWord == lineWrd):
                self.getAndSaveEmotion(line)
                break
        for switchedWord in self.getSwitched(self.word):
            if (switchedWord == lineWrd):
                self.getAndSaveEmotion(line)
                break
        if ((lineWrd in self.rawWord) or (lineWrd in self.word)): # jenom cast slova
            self.getAndSaveEmotion(line, True)
        return False

    def findEmotion(self):
        if self.word in WORDS:
            polarity, intensity = WORDS[self.word]
            if "eN" not in self.tags:
                self.getAndSaveEmotion(polarity, intensity)
            else:
                self.getAndSaveEmotion(self.negateEmotion(polarity), intensity)
            return
        if self.rawWord in WORDS:
            polarity, intensity = WORDS[self.rawWord]
            self.getAndSaveEmotion(polarity, intensity)
            return
        if self.rawWord.startswith("ne") and self.rawWord[2:] in WORDS:
            polarity, intensity = WORDS[self.rawWord[2:]]
            self.getAndSaveEmotion(self.negateEmotion(polarity), intensity)
            return
        switchedWords = self.getSwitched(self.word) + self.getSwitched(self.rawWord)
        for switchedWord in switchedWords:
            if switchedWord in WORDS:
                polarity, intensity = WORDS[switchedWord]
                self.getAndSaveEmotion(polarity, intensity)
                return
        """
        for file in FILES: #pro vsechny soubory s emocemi
            for line in file: #pro kazdy radek v souboru
                lineWrd = line.split(SEPARATOR, 1)[0]
                cntr = self.rawWord.count(' ')
                if (cntr == 1):
                    indexN = self.word.index(' ')
                    if (indexN > 0):
                        if((self.word[:indexN-1] in lineWrd) and (self.word[indexN+1:] in lineWrd)):
                            self.getAndSaveEmotion(line, lineWrd)
                            break
                else:
                    if self.noSpaceEmotion(line, lineWrd):
                        break
            for line in file: #pro kazdy radek v souboru
                lineWrd = line.split(SEPARATOR, 1)[0]
                cntr = self.rawWord.count(' ')
                if (cntr == 1):
                    indexR = self.rawWord.index(' ')
                    if (indexR > 0):
                        if((self.rawWord[:indexR-1] in lineWrd) and (self.rawWord[indexR+1:] in lineWrd)):
                            self.getAndSaveEmotion(line)
                            break
                else:
                    if self.noSpaceEmotion(line, lineWrd):
                        break
        """
        self.emotion.append('Z')
        self.emotionLevel.append(-1)

    def getSwitched(self, wrd):
        switchedWords = []
        switchedWords.append(wrd.replace('c', 'k'))
        switchedWords.append(wrd.replace('f', 'v'))
        switchedWords.append(wrd.replace('j', 'r'))
        switchedWords.append(wrd.replace('l', 'r'))
        switchedWords.append(wrd.replace('w', 'v'))
        switchedWords.append(wrd.replace('x', "ks"))
        switchedWords.append(wrd.replace('y', 'i'))
        switchedWords.append(wrd.replace('i', 'y'))
        switchedWords.append(wrd.replace('z', 's'))
        switchedWords.append(wrd.replace('s', 'z'))
        return switchedWords

    def negateEmotion(self, emotion):
        if emotion == "n":
            return "p"
        if emotion == "p":
            return "n"


    def getAndSaveEmotion(self, polarity, intensity, isComposed = False):
        self.emotion.append(polarity)
        if isComposed:
            self.emotionLevel.append(intensity / 2)
        else:
            self.emotionLevel.append(intensity)
        """
        for i,c in enumerate(line):
            if (c == SEPARATOR):
                if (isComposed and line[i+3] != '!'):
                    break
                if (line[i+1] == 'n' and self.rawWord[0]=='n' and self.rawWord[1]=='e'):
                    self.emotion.append('p') #uloz emoci
                elif (line[i+1] == 'p' and self.rawWord[0]=='n' and self.rawWord[1]=='e'):
                    self.emotion.append('n') #uloz emoci
                else:
                    self.emotion.append(line[i+1]) #uloz emoci
                if (isComposed):
                    self.emotionLevel.append(float(line[i+3])/2) #uloz koeficient(vahu slova)
                else:
                    self.emotionLevel.append(int(line[i+3])) #uloz koeficient(vahu slova)
                break
        """

if __name__ == "__main__":
    text = input(">>>")
    t = Text(text)
    t.printEmotions()
