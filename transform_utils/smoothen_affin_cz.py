#! /usr/bin/env python3


WORDS = set()
with open("slovesa_filtered.txt", "r") as f:
    for line in f:
        WORDS.add(line.strip())

AFFIN_LINES=[]

with open("affin_cz.csv", "r") as f:
    for line in f:
        word, polarity, intensity = line.strip().split(",")
        intensity = "0.5"
        AFFIN_LINES.append(f"{word},{polarity},{intensity}\n")

with open("affin_cz_smooth.csv", "w") as out_f:
    out_f.writelines(AFFIN_LINES)

