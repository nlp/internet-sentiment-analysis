#! /usr/bin/env python3

word_buffer = []

import sys

with open("afinn.cz.txt", "r") as f:
    for line in f:
        try:
            _, word, sentiment = line.split(",")
        except:
            print(line.split(","))
            continue
        if sentiment.strip() == "1":
            word_buffer.append(f"{word},p,1\n")
        elif sentiment.strip() == "-1":
            word_buffer.append(f"{word},n,1\n")

with open("affin_cz.csv", "w") as f:
    for line in word_buffer:
        f.write(line)
