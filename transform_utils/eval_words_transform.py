#! /usr/bin/env python3

word_buffer = []

with open("eval_words.txt", "r") as f:
    for line in f:
        word, positive_str, negative_str = line.split("\t")
        positive = float(positive_str)
        negative = float(negative_str)
        neutral = 1.0 - positive - negative
        if max(neutral, positive, negative) == neutral:
            continue
        if max(neutral, positive, negative) == positive:
            word_buffer.append(f"{word},p,{positive}\n")
        elif max(neutral, positive, negative) == negative:
            word_buffer.append(f"{word},n,{negative}\n")

with open("eval_words_translated.csv", "w") as f:
    for line in word_buffer:
        f.write(line)
